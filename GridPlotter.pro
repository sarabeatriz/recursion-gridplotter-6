#-------------------------------------------------
#
# Project created by QtCreator 2014-06-10T11:19:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GridPlotter
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    grid.cpp \
    tools.cpp

HEADERS  += dialog.h \
    grid.h

FORMS    += dialog.ui

RESOURCES += \
    images.qrc \
    styles.qrc
